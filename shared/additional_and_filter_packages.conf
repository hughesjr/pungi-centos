filter_packages = [
     ("^(BaseOS|AppStream|CRB|HighAvailability|ResilientStorage|SAP|SAPHANA)$", {
         "*": [
             "kernel-rt*", #RhBug 1973568
             "javapackages-bootstrap", #CS-636
         ]
     }),

     ("^.*$", {
         "*": [
             "glibc32",
             "libgcc32",
             "scap-security-guide-rule-playbooks",
             "*openh264*",      # https://fedoraproject.org/wiki/Non-distributable-rpms
             "python3-openipmi", #RhBug 1982794
             "OpenIPMI-perl", #RhBug 1982794
         ]
         "ppc64le": [
             "SLOF",
             "guestfs-tools",
             "libguestfs",
             "libvirt-daemon-kvm",
             "libvirt-daemon-driver-qemu",
             "qemu-kiwi",
             "qemu-kvm",
             "supermin",
             "virt-manager",
             "virt-v2v",
             "virt-p2v",
             "virt-top",
             "cockpit-machines",
         ],
       "s390x": [
             "rust-std-static-wasm32-unknown-unknown", #ENGCMP-1255
             "rust-std-static-wasm32-wasi",
         ],
     }),

]


additional_packages = [
    # Everything contains everything.
    ('^Everything$', {
        '*': [
            '*',
        ],
    }),
    ("^BaseOS$", {
        "*": [
            "liblockfile", #ENGCMP-2535
            "python3-gobject-base-noarch", #ENGCMP-2400
            "python3-samba-dc", #ENGCMP-3007
            "python3.9-debuginfo", #ENGCMP-1433, ENGCMP-2994
            "samba-tools", #ENGCMP-3007
            "samba-usershares", #ENGCMP-3007
            "subscription-manager-cockpit", #ENGCMP-2427
            "subscription-manager-rhsm-certificates", #ENGCMP-2357
            "kernel-modules-core", #ENGCMP-2899
            "kernel-debug-modules-core", #ENGCMP-2899
        ]
    }),
    ("^BaseOS$", {
         "aarch64": [
            "kernel-64k", #ENGCMP-2800
            "kernel-64k-core", #ENGCMP-2800
            "kernel-64k-debug", #ENGCMP-2800
            "kernel-64k-debug-core", #ENGCMP-2800
            "kernel-64k-debug-modules", #ENGCMP-2800
            "kernel-64k-debug-modules-core", #ENGCMP-2899, ENGCMP-2800
            "kernel-64k-debug-modules-extra", #ENGCMP-2800
            "kernel-64k-modules", #ENGCMP-2800
            "kernel-64k-modules-extra", #ENGCMP-2800
            "kernel-64k-modules-core", #ENGCMP-2899, ENGCMP-2800
         ],
         "ppc64le": [
         ],
         "x86_64": [
            "kernel-uki-virt", #ENGCMP-2899
            "kernel-debug-uki-virt", #ENGCMP-2899
         ],
         "s390x": [
            "kernel-zfcpdump-modules-core",
         ],
     }),
    ("^AppStream$", {
        "*": [
            "aardvark-dns", #ENGCMP-2515
            "adobe-source-code-pro-fonts", #ENGCMP-2390
            "alsa-plugins-pulseaudio", #ENGCMP-2359
            "aspnetcore-runtime-7.0", #ENGCMP-2586
            "aspnetcore-targeting-pack-7.0", #ENGCMP-2586
            "capstone", #ENGCMP-2591
            "dotnet-apphost-pack-7.0", #ENGCMP-2586
            "dotnet-host", #ENGCMP-2586
            "dotnet-hostfxr-7.0", #ENGCMP-2586
            "dotnet-runtime-7.0", #ENGCMP-2586
            "dotnet-sdk-7.0", #ENGCMP-2586
            "dotnet-targeting-pack-7.0", #ENGCMP-2586
            "dotnet-templates-7.0", #ENGCMP-2586
            "dpdk-devel", #ENGCMP-2205
            "ecj", #ENGCMP-2928
            "egl-utils", #ENGCMP-2476
            "firefox-x11", #ENGCMP-2806
            "freeglut-devel", #ENGCMP-2073
            "frr-selinux", #ENGCMP-2697
            "gcc-toolset-12", #ENGCMP-2391
            "gcc-toolset-12-annobin-annocheck", #ENGCMP-2384
            "gcc-toolset-12-annobin-docs", #ENGCMP-2384
            "gcc-toolset-12-annobin-plugin-gcc", #ENGCMP-2384
            "gcc-toolset-12-binutils", #ENGCMP-2415
            "gcc-toolset-12-binutils-devel", #ENGCMP-2415
            "gcc-toolset-12-binutils-gold", #ENGCMP-2415
            "gcc-toolset-12-build", #ENGCMP-2391
            "gcc-toolset-12-dwz", #ENGCMP-2402
            "gcc-toolset-12-gcc", #ENGCMP-2405
            "gcc-toolset-12-gcc-c++", #ENGCMP-2405
            "gcc-toolset-12-gcc-gfortran", #ENGCMP-2405
            "gcc-toolset-12-gcc-plugin-annobin", #ENGCMP-2805
            "gcc-toolset-12-gcc-plugin-devel", #ENGCMP-2405
            "gcc-toolset-12-gdb", #ENGCMP-2416
            "gcc-toolset-12-gdbserver", #ENGCMP-2416
            "gcc-toolset-12-libasan-devel", #ENGCMP-2405
            "gcc-toolset-12-libatomic-devel", #ENGCMP-2405
            "gcc-toolset-12-libgccjit", #ENGCMP-2405
            "gcc-toolset-12-libgccjit-devel", #ENGCMP-2405
            "gcc-toolset-12-libgccjit-docs", #ENGCMP-2405
            "gcc-toolset-12-libitm-devel", #ENGCMP-2405
            "gcc-toolset-12-liblsan-devel", #ENGCMP-2405
            "gcc-toolset-12-libquadmath-devel", #ENGCMP-2405
            "gcc-toolset-12-libstdc++-devel", #ENGCMP-2405
            "gcc-toolset-12-libstdc++-docs", #ENGCMP-2405
            "gcc-toolset-12-libtsan-devel", #ENGCMP-2405
            "gcc-toolset-12-libubsan-devel", #ENGCMP-2405
            "gcc-toolset-12-offload-nvptx", #ENGCMP-2405
            "gcc-toolset-12-runtime", #ENGCMP-2391
            "gcc-toolset-13-gdb", #ENGCMP-3129
            "gnome-kiosk-script-session", #ENGCMP-2499
            "gnome-kiosk-search-appliance", #ENGCMP-2499
            "google-crosextra-caladea-fonts", #ENGCMP-3140
            "gstreamer1-plugins-base-tools", #ENGCMP-2907
            "idm-pki-est", #ENGCMP-2798
            "ignition-edge", #ENGCMP-2770
            "ignition-validate", #ENGCMP-2656
            "jaxb-runtime", #ENGCMP-2881
            "jaxb-xjc", #ENGCMP-2881
            "keylime", #ENGCMP-2419
            "keylime-agent-rust", #ENGCMP-2420
            "keylime-base", #ENGCMP-2419
            "keylime-registrar", #ENGCMP-2419
            "keylime-selinux", #CS-1194
            "keylime-tenant", #ENGCMP-2419
            "keylime-verifier", #ENGCMP-2419
            "libasan8", #ENGCMP-2405
            "libblkio", #ENGCMP-3149
            "libgpiod", #ENGCMP-2433
            "libgpiod-devel", #ENGCMP-2433
            "libgpiod-utils", #ENGCMP-2433
            "libi2cd", #ENGCMP-2428
            "libi2cd-devel", #ENGCMP-2428
            "libnxz", #ENGCMP-2576
            "libsepol-utils", #ENGCMP-2399
            "libtsan2", #ENGCMP-2405
            "libvirt-daemon-common", # ENGCMP-3046
            "libvirt-daemon-lock", # ENGCMP-3046
            "libvirt-daemon-log", # ENGCMP-3046
            "libvirt-daemon-plugin-lockd", # ENGCMP-3046
            "libvirt-daemon-proxy", # ENGCMP-3046
            "libxcvt", #ENGCMP-2791 CS-1322
            "libzdnn", #ENGCMP-2244
            "libzdnn-devel", #ENGCMP-2297
            "man-db-cron", #ENGCMP-2595
            "mkpasswd", #ENGCMP-2259
            "mpdecimal", #ENGCMP-2828
            "netavark", #ENGCMP-2543
            "netstandard-targeting-pack-2.1", #ENGCMP-2586
            "nfsv4-client-utils", #ENGCMP-2493
            "nvme-stas", #ENGCMP-2495
            "libnvme", #ENGCMP-2358
            "passt", #ENGCMP-2741
            "passt-selinux", # ENGCMP-3074
            "pf-bb-config", #ENGCMP-2857
            "poppler-qt5", #ENGCMP-2393
            "pyproject-srpm-macros", #ENGCMP-2964
            "python3.11", #ENGCMP-2833
            "python3.11-cffi", #ENGCMP-2957
            "python3.11-charset-normalizer", #ENGCMP-2914
            "python3.11-cryptography", #ENGCMP-2958
            "python3.11-devel", #ENGCMP-2982
            "python3.11-idna", #ENGCMP-2888
            "python3.11-lxml", #ENGCMP-2954
            "python3.11-mod_wsgi", #ENGCMP-2955
            "python3.11-numpy", #ENGCMP-2933
            "python3.11-numpy-f2py", #ENGCMP-2982
            "python3.11-pip", #ENGCMP-2858
            "python3.11-ply", #ENGCMP-2911
            "python3.11-psycopg2", #ENGCMP-2956
            "python3.11-pycparser", #ENGCMP-2923
            "python3.11-PyMySQL", #ENGCMP-2963
            "python3.11-PyMySQL+rsa", #ENGCMP-2982
            "python3.11-pysocks", #ENGCMP-2912
            "python3.11-pyyaml", #ENGCMP-2910
            "python3.11-requests", #ENGCMP-2940
            "python3.11-requests+security", #ENGCMP-2982
            "python3.11-requests+socks", #ENGCMP-2982
            "python3.11-scipy", #ENGCMP-2979
            "python3.11-setuptools", #ENGCMP-2860
            "python3.11-six", #ENGCMP-2872
            "python3.11-tkinter", #ENGCMP-2982
            "python3.11-wheel", #ENGCMP-2873
            "python3-dnf-plugin-modulesync", #ENGCMP-2323
            "python3-alembic", #ENGCMP-2424
            "python3-greenlet", #ENGCMP-2421
            "python3-keylime", #ENGCMP-2419
            "python3-lark-parser", #ENGCMP-2422
            "python3-lasso", #ENGCMP-2742
            "python3-i2c-tools", #RHBZ#2072719
            "python3-libgpiod", #ENGCMP-2433
            "python3-libnvme", #ENGCMP-2412
            "python3-pyqt5-sip", #ENGCMP-2370
            "python3-sqlalchemy", #ENGCMP-2423
            "python3-tomli", #ENGCMP-3044 CS-1485
            "python3-tornado", #ENGCMP-2418
            "python3.11-urllib3", #ENGCMP-2932
            "python3-virt-firmware", #ENGCMP-2726
            "python3-wcwidth", #ENGCMP-2093
            "qatlib-service", #ENGCMP-2490
            "redhat-cloud-client-configuration", #ENGCMP-2401
            "rtla", #ENGCMP-2799
            "rust-analyzer", #ENGCMP-2839
            "sip6", #ENGCMP-2239
            "sssd-idp", #ENGCMP-2276
            "synce4l", #ENGCMP-2794
            "system-backgrounds",
            "tomcat", #ENGCMP-2927
            "tomcat-admin-webapps", #ENGCMP-2927
            "tomcat-docs-webapp", #ENGCMP-2927
            "tomcat-el-3.0-api", #ENGCMP-2927
            "tomcat-jsp-2.3-api", #ENGCMP-2927
            "tomcat-lib", #ENGCMP-2927
            "tomcat-servlet-4.0-api", #ENGCMP-2927
            "tomcat-webapps", #ENGCMP-2927
            "tuned-profiles-postgresql", #ENGCMP-2126
            "usbredir-server", #ENGCMP-2719
            "xdg-desktop-portal-gnome", #ENGCMP-2146
            "xmlstarlet", #ENGCMP-2296
            "xxhash", #ENGCMP-2455
            "xxhash-libs", #ENGCMP-2455
            "yara", #ENGCMP-2372
        ]
    }),
    ("^AppStream$", {
         "x86_64": [
             "cxl-cli", #ENGCMP-2743
             "cxl-libs", #ENGCMP-2743
             "libreoffice", #ENGCMP-2968
             "open-vm-tools-salt-minion", #ENGCMP-2295
             "virt-dib",
             "vorbis-tools",
         ],
         "aarch64": [
             "virt-dib",
             "kernel-64k-debug-devel", #ENGCMP-2800
             "kernel-64k-debug-devel-matched", #ENGCMP-2800
             "kernel-64k-devel", #ENGCMP-2800
             "kernel-64k-devel-matched", #ENGCMP-2800
             "rhc-worker-playbook", #ENGCMP-3127
         ],
         "s390x": [
             "libzpc", #ENGCMP-2756
             "virt-dib",
         ],
         "ppc64le": [
             "vorbis-tools",
             "libreoffice", #ENGCMP-2968
         ],

     }),
    ("^CRB$", {
        "*": [
         ],
        "x86_64": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "aarch64": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "s390x": [
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "ppc64le": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
         ],
     }),

    ("^SAP$", {
        "*": [
            "compat-sap-c++-12", # ENGCMP-2843
        ]
    }),

    ("^SAPHANA$", {
        "*": [
            "compat-sap-c++-12", # ENGCMP-2843
        ]
    }),

    ("^Buildroot$", {
        "*": [
            "*",
        ]
    }),
]
